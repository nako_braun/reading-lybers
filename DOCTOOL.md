## Récupérer l'HTML d'un lyber et lui appliquer un style CSS 
1. Enregistrer la page sous le format HTML.
2. Ouvrir la page HTML dans Atom puis appliquer « Beautiful editor contents », cela permet de bien restructurer le HTML. 

**Observations**
- « beautiful editor contents » ajoute parfois < !-- juste avant la fermeture de la balise </style> du css média, pas dérangeant en soi...
- « beautiful editor contents » ne fonctionne pas parfois, peut être parce que le code est trop conséquent ?
- on remarque que le style CSS associé à la page web du lyber est intégré dans la page HTML.

3. Ajouter `media="screen"`pour le style CSS présent dans la page HTML.
4. Ajouter dans la balise `<head></head>` notre CSSPrint : `<link rel="stylesheet" type="text/css" href="impression.css" media="print">`

## Structures des pages HTML

### Dans nos histoires
```html 
h1 : titre de l’œuvre
	<h1></h1>
h2 : chapitres/parties
	<h2 id=""><span></span></h2>
h3 : sous-chapitres/chapitres

ul class="table" : table des matières/sommaire
	<h2><span>table des matières</span></h2>
		<ul class="table">
			<li><a href=""></a></li>

<span class="spip_note_ref"></span> : notes dans le texte
	<span class="spip_note_ref">[<a href="" class="spip_note" rel="footnote" title="" id=""></a>]</span>

<p><span class="spip_note_ref"></span></p> : notes
	<p><span class="spip_note_ref">[<a href="" class="spip_note" rel="footnote" title="" id=""></a>]</span></p>

class="spip" : dialogues
<blockquote class="spip"></blockquote>

<i></i> : ??

<ul class="spip"><li></li></ul> : énumération avec des points 
```

### Éditions zones (la plupart ont cette structure)
```html
<section class="chap"></section> : chapitres + sous-chapitres + notes
	<section class="chap" epub:type="chapter">
		ou  <section class="chapitre" epub:type="chapter">
			<div class="chapitre">

class="sommaire" : sommaire
	<div class="sommaire"><a href""=></a> : sommaire

h1 : chapitres
	<h1 epub:type="title"></h1>
	<h1 epub:type="subtitle"></h1>

h2 : sous-chapitres
	<h2 epub:type="title"><h2>

h3 : sous-sous-chapitres
	<h3 epub:type="title"></h3>

<a class="apnb" epub:type="noteref"></a> : notes dans le texte

class="defnotes" : notes
à la fin d’une <section class="chap"></section>
		<section class="defnotes" epub:type="footnotes">
			<aside class="ntb" epub:type="footnote"></aside>

<span epub:type="pagebreak"></span> : ??
```
**Observations**
- Plusieurs balises epub : note de bas de page: `epub:type="footnote"`, note de fin de texte: `epub:type="endnote"`, section des notes de fin de page: `<section epub:type="endnotes">`
- Ces livres existent en format epub de base puis sont convertis en html pour les lybers ?

### Éditions de l’éclat
Plusieurs pages à télécharger pour un même lyber.

## Outils HtmltoPrint

- Css Paged Media : <https://www.w3.org/TR/css-page-3/#intro>
- Ether2html : <http://osp.kitchen/tools/ether2html/>
- Paged.js : <https://www.pagedjs.org/>
- WeasyPrint : <https://weasyprint.org/> + <https://github.com/Kozea/WeasyPrint>, logiciel libre et open source pour faire rapidement du print à partir d'un html 

### Paged.js
À propos : "Paged.js est une bibliothèque JavaScript gratuite et open source qui pagine le contenu dans le navigateur pour créer une sortie PDF à partir de n'importe quel contenu HTML. Cela signifie que vous pouvez concevoir des ouvrages pour l'impression (par exemple, des livres) en utilisant HTML et CSS ! Paged.js suit les normes Paged Media publiées par le W3C (c'est-à-dire le module Paged Media et le module Generated Content for Paged Media). En effet, Paged.js agit comme un polyfill pour les modules CSS afin d'imprimer du contenu en utilisant des fonctionnalités qui ne sont pas encore supportées nativement par les navigateurs."

**Comment ça marche**

On peut sauvegarder les scripts suivants dans notre projet : 
- script paged.js : <https://unpkg.com/browse/pagedjs@0.2.0/dist/paged.js>
- script paged.js polyfill.js : <https://unpkg.com/browse/pagedjs@0.2.0/dist/paged.polyfill.js>

Mais si on travaille depuis un localhost il suffit d'appeler le script et d'ajouter le style suivant dans l'html : 
```html 
<head>
	<!-- CSS paged.js -->
	<style type="text/css" media="screen">
	:root {
		--color-background: whitesmoke;
		--color-pageBox: #666;
		--color-paper: white;
		--color-marginBox: transparent
	}

	@media screen {
		body {
			background-color: var(--color-background)
		}

		.pagedjs_pages {
			display: flex;
			width: calc(var(--pagedjs-width) * 2);
			flex: 0;
			flex-wrap: wrap;
			margin: 0 auto
		}

		.pagedjs_page {
			background-color: var(--color-paper);
			box-shadow: 0 0 0 1px var(--color-pageBox);
			margin: 0;
			flex-shrink: 0;
			flex-grow: 0;
			margin-top: 10mm
		}

		.pagedjs_first_page {
			margin-left: var(--pagedjs-width)
		}

		.pagedjs_page:last-of-type {
			margin-bottom: 10mm
		}

		.pagedjs_margin-bottom,
		.pagedjs_margin-bottom-center,
		.pagedjs_margin-bottom-left,
		.pagedjs_margin-bottom-left-corner-holder,
		.pagedjs_margin-bottom-right,
		.pagedjs_margin-bottom-right-corner-holder,
		.pagedjs_margin-left,
		.pagedjs_margin-left-bottom,
		.pagedjs_margin-left-middle,
		.pagedjs_margin-left-top,
		.pagedjs_margin-right,
		.pagedjs_margin-right-bottom,
		.pagedjs_margin-right-middle,
		.pagedjs_margin-right-top,
		.pagedjs_margin-top,
		.pagedjs_margin-top-center,
		.pagedjs_margin-top-left,
		.pagedjs_margin-top-left-corner-holder,
		.pagedjs_margin-top-right,
		.pagedjs_margin-top-right-corner-holder {
			box-shadow: 0 0 0 1px inset var(--color-marginBox)
		}
	}
	</style>
	<script src="https://unpkg.com/pagedjs/dist/paged.polyfill.js"></script>
</head>
```
**Outils intéréssants**
- génère un footer en fonction du chapitre en question : <https://www.pagedjs.org/documentation/07-generated-content-in-margin-boxes/#named-string-classical-running-headersfooters>
- numéroter les pages : <https://www.w3.org/TR/css-page-3/#page-based-counters>
- notes : nouvelle balise `<note></note>` <https://www.pagedjs.org/posts/2020-05-13-notes-about-notes/>

**Problèmes rencontrés**
- numérotation des pages fonctionne mais comment on enleve la numérotation sur certaine page comme la page de couverture par exemple ?

### Problèmes de CSS liés au navigateur
- `orphans: 3;` orphelin = première ligne d'un paragraphe qui apparaît seul en bas d'une page, ne fonctionne pas sur Firefox
- `widows: 3;` veuve = dernière ligne d'un paragraphe qui apparaît seule en haut d'une page, ne fonctionne pas sur Firefox
- `hyphens: auto;` césure, fonctionne que sur Firefox
- `hyphenate-limit-chars: 10 3 2;` césure, fonctionne que sur Edge et Internet Explorer <https://openweb.eu.org/articles/la-gestion-de-la-cesure-en-css>

### Intégrer une typographie dans un HTML
Récuperer base64 sur Fontsquirrel <https://www.fontsquirrel.com/> en générator et en mode expert.

### Impression
<https://openweb.eu.org/articles/maitriser_impression_css> Avec Internet explorer et Firefox on peut positionner le titre de la page, l’URL, la date/heure d’impression, le numéro de la page courante et le numéro de la page par rapport au nombre total de pages <https://www.pcastuces.com/pratique/astuces/1418.htm> Avec Opéra et Internet Explorer il est possible d'ajuster la taille de la fonte dans l’aperçu avant impression.
