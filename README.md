# Reading Lybers

_Reading Lybers_ est un outil numérique en cours de développement qui permet de mettre en page rapidement des lybers sous différents formats. Ce projet s’inscrit dans une recherche autour de _La littérature en environnement numérique_ : il est question d’interroger l’évolution de la littérature depuis l’apparition du numérique, que ce soit dans sa forme, son sens, ou encore son usage. 

Vous trouverez ma bibliographie complète en ligne sur <https://www.zotero.org/nataliapageau/library>

**Objectifs de la recherche**
- modifier la mise en page d’un lyber pour qu’il soit agréable à la lecture
- créer un outil qui aide à la recherche dans le cadre universitaire./multiversitaire ?

**Objectifs techniques**
- l'utilisateur doit seulement informer l'url du lyber
- déployer différens formats de lectures 
- automatiser l’imposition (calcul des pages, page de début/page de fin, ajout de pages si le total des pages n’ai pas un multiple de 4 etc.)

## Lyber
Petit traité explicatif de ce qu'est un lyber par Michel Valensi: <http://www.lyber-eclat.net/lyber/lybertxt.html>

**Résumé**

Le mot « lyber » provient de « Liber » en latin, divinité ivre de la vigne ce qui voulait dire aussi : libre, enfant, vin, livre et désignait également la partie vivante de l’écorce des arbres. Cette divinité qu'on assimilera plus tard à Bacchus, avait la particularité de ne pas avoir de temple, et se trouvait donc consacrée partout où étaient susceptibles de s’assembler un nombre suffisant de personnes pour la fêter. 

Un lyber est une proposition de cohabitation entre de deux supports, le livre papier et le livre numérique. Chaque édition textuelle imprimée aurait un double disponible dans son intégralité et gratuitement sur le Net. 
Cette idée a été proposé en 2000 par Michel Valensi, éditeur des éditions Eclat, à la suite d’une polémique en France sur le prêt payant en bibliothèque. Selon Michel Valensi, il n’existe pas quelque chose capable de substituer au livre, et encore moins les machines. Cependant, depuis l’apparition du numérique, il explique que les éditeurs sont contraints de reconsidérer la question des supports. Notamment parce que les premières éditions numériques ont été à l’initiative de directeurs commerciaux s’improvisant directeurs littéraires. La question qu’il se pose est la suivante : Comment ces deux supports peuvent-ils cohabiter et quel statut donner à l’un et à l’autre ?

Ce qui concerne la question du droit d’auteur, Michel Valensi répond que dans tout acte de création il existe un moment de don qui fonde cet acte, et que tout auteur.ice veut avant tout être lu.e. Il a également fait des statistiques au sein de sa maison d’édition qui prouverait que le lyber a permis de faire des ventes pour les livres qui stagnaient. De plus ces statistiques montreraient également que les ouvrages les plus vendus sont ceux qui sont les plus consultés. 
Il pose également la question du droit de lecteur et présente le lyber comme un moyen non inefficace pour lutter contre les « livres d’une heure ». Cela permettrait alors à ce genre de livre de ne plus avoir de raison d’être en librairie.

**Les particularités :**
- pouvoir essayer un produit avant de l’acheter, Michel Valensi fait une analogie avec la radio et l’achat des CD. 
- une invitation à acheter le livre papier : l’achat ne serait plus seulement pour soi mais plutôt pour l’autre, dans l’idée de faire partager le savoir. 
- la possibilité de signaler l’adresse du libraire le plus proche du domicile.
- la possibilité aux lecteurs d’intervenir en commentaires sur le texte en ligne avec des créations de fichiers complémentaires.

**La question des licences**
Le site des Editions de l’éclat est sous licence CC (BY NC SA) et les lybers sont sous licence LYBER (grand-père de l’open edition)
<http://www.lyber-eclat.net/lyber/licence.html> <http://www.lyber-eclat.net/contacts/>

## Maisons d'éditions qui proposent des lybers:
- Dans nos histoires : <https://lyber.dansnoshistoires.org/> (format HTML + css print existant?)
- Éditions zones : <https://www.editions-zones.fr/livres/> (format HTML)
- Éditions de l’éclat : <http://www.lyber-eclat.net/les-lybers-disponibles/> (format HTML + PDF)

## Bibliographie
<https://www.zotero.org/nataliapageau/collections/W4R9AQDL>

- Bouzet, Ange-Dominique, _Après la pétition en faveur du prêt payant des livres. Polémique en bibliothèque. Les pouvoirs publics vont devoir arbitrer le vif conflit qui oppose auteurs et bibliothécaires._, Libération, section Livres <https://www.liberation.fr/evenement/2000/04/22/apres-la-petition-en-faveur-du-pret-payant-des-livres-polemique-en-bibliotheque-les-pouvoirs-publics_321933/> 
- _Libres enfants du savoir numérique_ <https://www.cairn.info/libres-enfants-du-savoir-numerique--9782841620432.htm> 
- Valensi, Michel, _Petit Traité Plié En Dix Sur Le Lyber_ <http://www.lyber-eclat.net/lyber/lybertxt.html>
- Valensi, Michel, _Retour sur le Lyber_, 2004 <https://www.cairn.info/revue-multitudes-2004-5-page-161.htm>
