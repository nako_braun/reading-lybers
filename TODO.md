# Théorie
- [ ] demander au Rideau de Perles si interessé.e par l'outil + voir également avec Peagy comment mettre en place le dispositif
- [ ] rédiger texte explicatif de la responsabilité de chacun pour l'impression des textes 

### Interview 
- [ ] interview -> Alexia me met en lien avec nom?
- [ ] préparer mails + appeler maisons d'éditions
- [ ] préparer les questions -> voir INTERVIEW.md maisons d'éditions
- [ ] contacter les trois maisons d'éditions pour une demande d'interview

### Proposer une expérimentation de lecture qui facilite la recherche
voir proposition que je fais "dans visualisation pdf proposer :" dans "CsstoPrint/Paged.js"
- [ ] lister toutes les nouvelles formes d'outils numériques qui existent pour le livre numérique
- [ ] lié à ma méthodologie de travail

# Outil

## Fichier book.php
- [ ] cleaner le php

### CSStoPrint/Paged.js
**Pour toutes les maisons d'éditions**
- [ ] dans visualisation pdf proposer : rechercher des mots (titres également) dans le texte, rechercher la page exacte, changer la typo du texte et/ou des titres, changer la taille de la typo en direct
- [ ] marge plus petite au centre de la page !
- [ ] cleaner le css
- [ ] césures texte justifié -> voir <https://korutx.github.io/js/Hyphenopoly/>
- [ ] revoir le sommaire css
- [ ] chapitres css en fonction de s'il y a des h3, h4, etc ou pas
- [x] @top-center white-space: nowrap;
- [x] numérotation, enlever la numérotation sur la page de couverture -> voir avec Louise
- [x] enlever la numérotation sur la dernière page et enlever le titre 
- [x] utiliser le script suivant pour les les espaces en espaces fines etc <https://gitlab.com/JulieBlanc/typesetting-tools?fbclid=IwAR29LHig-KHh9aocUzC-v5VM277SJRjXXzYbJWGcYyt7JOxAIewlos6BO-Q>

**Dans nos histoires**
- [ ] chapitre enlever le mot à chaque fois ?
- [ ] résumé du texte sur la dernière page -> extraire de la page d'avant
- [ ] sub-chapter aller a la ligne quand ce n'est pas ap un chapter
- [ ] citation ap table des matières text-align:center
- [x] page break left à règler marche pas "À l’écoute des souvenirs"
- [x] table des matières avec un T majuscule
- [x] h4 : enlever les `<br>`
- [x] notes de bas de page
- [x] h2 enlever guillemets
- [x] h2>li enlever guillemets
- [x] css `<sup></sup>` et `<i></i>`

**Editions Zones**

Pour le premier type d'html :
- [ ] notes de bas de page avec "note"
- [ ] auteur.rice espace entre nom et prénom
- [ ] sous-titre text indent
- [ ] tableau ou autre chelou à regler
- [x] sommaire
- [x] notes de bas de page avec "chiffres"

Pour le second type d'html :
- [ ] faire css

**Editions Éclats**
- [ ] faire le css

## Fichier index.php
- [ ] afficher les langages utilisés, typo, etc
- [ ] regler le pb responsive input pdf
- [ ] faire message d'erreur quand url n'ai pas correcte

### Formats
**Proposer différents exports**
- [ ] dans visualisation pdf proposer : export pdf entier
- [ ] dans visualisation pdf proposer : export pdf extrait proposer impression cahier (doit être un multiple de 4) ou impression recto verso (pas de limite de page)
- [ ] txt
- [ ] epub
- [x] visualisation du pdf simplement avec paged.js

### Impression
- [ ] ajouter page blanche css première et dernière page ??? 
- [ ] imposition avec Pdfjam test -> Léo
- [ ] possibilité d'imprimer des extraits -> avec couv + table des matières inclut + préciser que c'est un "extrait"
- [ ] se renseigner sur la possibilité de restreindre le nombre de page d'impression ?
- [x] faire un test d'impression
