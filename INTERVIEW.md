# Interview

Séverine Dusollier -> mail envoyé le 22/02/22

## Maisons d'éditions

### Éditions Zones

**Récapitulatif**

Mail envoyé, 2 réponses. 
Marieke Joly, Editrice, Editions La Découverte/Collection "Zones" -> RDV 15 février à 15h -> RETRANSCRIPTION EN COURS <https://pads.erg.be/p/EditionsZones>
Lysis Mettens, Relations libraires / Chargée de communication numérique, Éditions La Découverte -> répondre mail et envoyer questions ? Pas nécessaire pour le moment

En 2007, les éditions La Découverte crée le label Zones, un label dirigé par Grégoire Chamayou.

Les éditions La Découverte propose des livres numériques payant. Iels donnent des extrait à lire de type, pages scannées du livre, via la plateforme Calaméo. Calaméo est un service en ligne qui permet la conversion gratuite de documents en publications numériques. 

Zones combine la publication commerciale classique sur papier et la diffusion en libre accès sur Internet, en mettant gratuitement et intégralement en ligne la plupart de ses titres, sur le principe du Lyber initié en France par les éditions de l’Éclat : "Vous avez ici gratuitement accès au contenu des livres publiés par Zones. Nous espérons que ces lybers vous donneront envie d'acheter nos livres, disponibles dans toutes les bonnes librairies. Car c'est la vente de livres qui permet de rémunérer l'auteur, l'éditeur et le libraire, et de vous proposer de nouveaux lybers et de nouveaux livres."

Tarteaucitron.js est un script open-source léger qui s’implémente sur un site afin de donner au visiteur le choix d’accepter ou de refuser certains cookies de manière individuelle.

**Questions Joly**

- Pourquoi Zones a été créé au sein des Éditions La Decouverte, et comment se fait-il que les formats de lecture numérique proposés ne sont-ils pas identiques chez Zones et La Découverte (plateforme Calaméo) ? Quelle est votre position quant aux Editions La Découverte ?
- J'ai vu que FéminiSpunk de Christine Aventin est également en vente aux éditions Découverte seulement le lyber n'est pas accessible pourquoi ? <https://www.editionsladecouverte.fr/feminispunk-9782355221651>
- Pour quelles raisons (politique, sociale etc.) Zones a choisi de proposer une lecture intégrale de ses livres sur le Net?
- Comment avez-vous connu le format lyber et pourquoi avez-vous choisi ce format plutot qu'un autre ? 

Michel Valensi pense que rien ne peut substituer au livre papier. Cependant depuis l’apparition du numérique, il explique que les éditeurs sont contraints de reconsidérer la question des supports. Notamment parce que les premières éditions numériques ont été à l’initiative de directeurs commerciaux s’improvisant directeurs littéraires. La question qu’il se pose est la suivante : Comment ces deux supports peuvent-ils cohabiter et quel statut donner à l’un et à l’autre ?
- Êtes-vous d'accord avec cette idée que rien ne peut substituer au livre papier et pourquoi ?
- Que pensez-vous des nouveaux usages développés dans les livres numériques, que ce soit les liseuses et d'autres expérimentations textuelles ? (exemple : changer la taille de la police, possibilité de faire des annotations, etc.)
- Pensez-vous que le lyber pourrait être appliqué de manière globale dans le milieu de l'édition ? 

Licence d'un lyber écrit par Michel 
"Ce lyber peut être téléchargé, imprimé, agrafé, relié, etc. (à la mesure de votre habileté et de vos facultés manuelles), etc. L'éditeur est le « gardien du copyright » de ce lyber. Il veillera à ce que soient arrêté(e)s dans leur élan les pilleurs et pilleuses de littérature qui abondent aujourd'hui pour pouvoir pondre leur roman de "rentrée littéraire" avant d'être absorbé(e)s par leur propre néant... (on ne vit vraiment pas une époque formidable!)" <http://www.lyber-eclat.net/lyber/licence.html>. 
- Y-a-t il une licence appliqué pour vos lybers ? Si oui comment s'associe-t-elle avec les droits d'auteurs existants ?
- Comment introduisez-vous cette initiative à vos auteur.rice.s ? 
- Est-ce que des auteur.rice.s vous contactes pour ces raisons principalement ?
- Faites-vous des statistiques comme Michel Valensi pour vérifier l'efficacité de ce support ? Les liens sont-ils fortement consultés ?
- Pensez-vous que les lybers permettent de booster les ventes des livres papiers ?
- Est-ce pour vous il s'agit d'une stratégie commerciale que vos textes ne soit pas agréable à la lecture sur le Net ?
- Avez-vous eu des retours de client.e.s/utilisateur.rice.s qu'ils soient positifs ou négatifs sur les lybers et lesquels sont-ils ?
- Dans le cadre de mes études scolaire, je suis actuellement en train de travailler sur un outil permettant de remettre en page facilement vos lybers. Il s'agit d'un outil que je compte proposer à disposition des étudiant.e.s de mon école. Que pensez-vous de cette initiative ? 

En analysant votre code, je vois qu'il existe des balises
- Est-ce à partir de vos epub que vos lybers sont générés ? 
- Comment se fait-il, qu'il existe deux formes d'html différentes dans votre catalogue de lybers ?


**Questions Lysis**

### Éditions Dans Nos Histoires

**Récapitulatif**

Mail envoyé. Réponse étrange "choix anciens..?" -> recontacter plus tard

Eitions toujours en activité. _Créature de rêve, une aventure d'Alys_ parution juin 2021

### Éditions de l'Éclat

**Récapitulatif**

Mail à envoyer !
Lire les interview pour préparer mon mail.

Michel Valensi écrit *Petit traité plié en dix du lyber* en 2000

"Depuis l’apparition du numérique, les éditeurs sont contraints de reconsidérer la question des supports"
