<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
      <title>Reading Lybers</title>
      <link rel="stylesheet" type="text/css" href="./css/index.css">
  </head>

  <body>
   <div class="container">
      <section class="textIntro">
        <h2><i>Reading Lybers<i></h2>
        <p><i>Reading Lybers</i> est un outil numérique en cours de développement qui permet de mettre en page rapidement des lybers des <a href="https://dansnoshistoires.org/" target="_blank">éditions dans nos histoires</a>, <a href="https://www.editions-zones.fr/" target="_blank">éditions zones</a>, et des <a href="https://www.lyber-eclat.net/les-lybers-disponibles/" target="_blank">éditions de l'éclat</a>, sous différents formats. <br>
          Ce projet s’inscrit dans le cadre d'une recherche autour de <i>La littérature en environnement numérique</i>&thinsp;: il est question d’interroger l’évolution de la littérature depuis l’apparition du numérique, que ce soit dans sa forme, son sens, ou encore son usage. <br>
          Cet outil est développé par Natalia Pageau. Il est accesible en ligne sur GitLab&thinsp;: <a href="https://gitlab.com/nako_braun/reading-lybers" target="_blank">https://gitlab.com/nako_braun/reading-lybers</a>
        </p>
      </section>

      <section class="input">
        <!-- <h3>Entrez ici l'url de votre lyber :</h3> -->
          <form action="book.php" method="post">
            <ul>
              <li>Entrez ici l'url de votre lyber&thinsp;: <input name="link" type="url"/></li>
              <li><input name="button" type="submit" value="visualiser le fichier pdf"/></li>
            </ul>
          </form>
      </section>
  </div>
  <footer>
  </footer>
  </body>

</html>
