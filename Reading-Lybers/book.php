<?php
  // echo phpinfo();
  //
  // ini_set('display_errors', 1);
  // ini_set('display_startup_errors', 1);
  // error_reporting(E_ALL);

  include 'inc/header.php';

  function dansNosHistoires($dom){
    $contenu = $dom->getElementById('contenu');
    $finder = new DomXPath($dom);

    /* Notes de bas de page */
    $classname = "spip_note_ref";
    $notes = $finder->query("//*[contains(@class, '$classname')]");

    foreach ($notes as $note) {
      $footnote = $dom->createDocumentFragment();
      $noteLinks = $note->getElementsByTagName('a');

      foreach($noteLinks as $noteLink){
        $href = $noteLink->getAttribute('href');

        if(preg_match('!#nh[0-9]+!', $href))
          break;

        $noteContent = $dom->getElementById(str_replace('#', '', $noteLink->getAttribute('href')));
        $spans = $noteContent->getElementsByTagName('span');

        foreach($spans as $span){
          $span->parentNode->removeChild($span);
        }

        $footnote->appendXml('<span class="footnote">'.$noteContent->textContent.'</span>');
        $noteContent->parentNode->removeChild($noteContent);
        $note->parentNode->replaceChild($footnote, $note);
      }
    }

    /* Guillemets */
    $guillemets = ["«", "»"];
    $replacement = [""];

    $parts = $finder->query("//h2");

    foreach ($parts as $part) {
      $spansPart = $part->getElementsByTagName('span');
      $newSpanPart = $dom->createDocumentFragment();

      foreach($spansPart as $spanPart){
        $contentPart = $spanPart->textContent;

        if(preg_match('!«!', $contentPart)) {
          $newContentPart = str_replace($guillemets, $replacement, $contentPart);
          $newSpanPart->appendXml('<span>'.$newContentPart.'</span>');
          $spanPart->parentNode->replaceChild($newSpanPart, $spanPart);
        } else {}
      }
    }

    $classname2 = "table";
    $summaryChapters = $finder->query("//*[contains(@class, '$classname2')]");

    foreach ($summaryChapters as $summaryChapter) {
      $summaryChapterLis = $summaryChapter->getElementsByTagName('li');
      $newSummaryChapterLi = $dom->createDocumentFragment();

      foreach ($summaryChapterLis as $summaryChapterLi) {
        $summaryChapterContent = $summaryChapterLi->textContent;

        if(preg_match('!«!', $summaryChapterContent)) {
          $newSummaryChapteContent = str_replace($guillemets, $replacement, $summaryChapterContent);
          $newSummaryChapterLi->appendXml('<li>'.$newSummaryChapteContent.'</li>');
          $summaryChapterLi->parentNode->replaceChild($newSummaryChapterLi, $summaryChapterLi);
        } else {}
      }
    }

    /* enlever les balises <br> dans les titres h4 */
    $subChapters = $finder->query("//h4");

    foreach ($subChapters as $subChapter) {
      $newSubChapter = $dom->createDocumentFragment();
      $brsSubChapter = $subChapter->getElementsByTagName('br');

      foreach ($brsSubChapter as $brSubChapter) {
        $newSubChapter->appendXml(' ');
        $brSubChapter->parentNode->replaceChild($newSubChapter, $brSubChapter);
      }
    }

    /* enlever les ancres inutle */
    $classname3 = "ancre";
    $ancres = $finder->query("//*[contains(@class, '$classname3')]");

    foreach ($ancres as $ancre) {
      $ancre->parentNode->removeChild($ancre);
    }

    $classname4 = "not_print";
    $notPrints = $finder->query("//*[contains(@class, '$classname4')]");

    foreach ($notPrints as $notPrint) {
      $notPrint->parentNode->removeChild($notPrint);
    }

    /* enlever la partie notes */
    $html = $contenu->C14N();
    $html = str_replace('<h2><span>notes</span></h2>', '', $html);

    return $html;
  }

  function editionsZones($dom){
    $body = $dom->getElementsByTagName('body')[0];
    $finder = new DomXPath($dom);

    /* Notes de bas de page */
    $classname1 = "apnb";
    $notes = $finder->query("//*[contains(@class, '$classname1')]");

    foreach ($notes as $note) {
      $footnote = $dom->createDocumentFragment();
      $href = $note->getAttribute('href');
      $noteContent = $dom->getElementById(str_replace('#', '', $href));
      $noteText = $noteContent->textContent;

      $noteLinks = $noteContent->getElementsByTagName('a');

      foreach($noteLinks as $noteLink){
        $noteLink->parentNode->removeChild($noteLink);
      }

      $noteText = preg_replace('$^[0-9]\.+$', '', $noteText);
      $noteText = preg_replace('$^[0-9]+$', '', $noteText);
      $noteText = preg_replace('$\.$', '', $noteText);

      $footnote->appendXml('<span class="footnote">'.$noteText.'</span>');
      // $noteContent->parentNode->removeChild($noteContent);
      $note->parentNode->replaceChild($footnote, $note);
    }

    // $finder = new DomXPath($dom);
    //
    // /* First page */
    // $classname1 = "pagetitre";
    // $elementsTitle = $finder->query("//*[contains(@class, '$classname1')]");
    //
    // foreach ($elementsTitle as $elementTitle) {
    //   $newElementTitle = $dom->createDocumentFragment();
    //   $elementTitleClasss = $elementTitle->getAttribute('class');
    //   // echo $elementTitleClasss;
    //
    //   $contentElementTitle = str_replace("ident", "", $elementTitleClasss);
    //   $newElementTitle->appendXml('<class = "'.$contentElementTitle.'"></class>');
    //   // echo $newElementTitle;
    //   $elementTitle->parentNode->replaceChild($newElementTitle, $elementTitle);
    // }

    return $body->C14N();
  }
?>

<main>
  <?php

    $url = $link;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    curl_close($ch);

    $dom = new DomDocument();
    $dom->loadHTML($data);

    switch($routine){
      case 0:
        echo dansNosHistoires($dom);
        break;
      case 1:
        echo editionsZones($dom);
        break;
      case 2:
        echo editionsEclats($dom);
    }

  ?>
</main>

<?php
  @include 'inc/footer.php';
?>
