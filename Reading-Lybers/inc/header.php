<?php
  $link = $_POST['link'];
  $routines = array('dansnoshistoires', 'editions-zones', 'lyber-eclat');
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">

      <title>Reading Lybers</title>

      <!-- CSS -->
      <?php
        if (preg_match("/dansnoshistoires|editions-zones|lyber-eclat/i", $link, $matches)) {
          $routine = array_search($matches[0], $routines);
          ?>
            <link rel="stylesheet" type="text/css" href="./css/<?php echo $matches[0]; ?>.css">
          <?php
        } else {}


      ?>

      <!-- CSS paged.js remix -->
      <style type="text/css" media="screen">
      	:root {
      		--color-background: whitesmoke;
      		--color-pageBox: #666;
      		--color-paper: white;
      		--color-marginBox: transparent
      	}

      	@media screen {
      		body {
      			background-color: #e2dde4;
      		}

      		.pagedjs_pages {
      			display: flex;
      			width: calc(var(--pagedjs-width) * 2);
      			flex: 0;
      			flex-wrap: wrap;
      			margin: 0 auto
      		}

      		.pagedjs_page {
      			background-color: var(--color-paper);
      			box-shadow: 0 0 0 1px var(--color-pageBox);
      			margin: 0;
      			flex-shrink: 0;
      			flex-grow: 0;
      			margin-top: 10mm
      		}

      		.pagedjs_first_page {
      			margin-left: var(--pagedjs-width)
      		}

      		.pagedjs_page:last-of-type {
      			margin-bottom: 10mm
      		}

      		.pagedjs_margin-bottom,
      		.pagedjs_margin-bottom-center,
      		.pagedjs_margin-bottom-left,
      		.pagedjs_margin-bottom-left-corner-holder,
      		.pagedjs_margin-bottom-right,
      		.pagedjs_margin-bottom-right-corner-holder,
      		.pagedjs_margin-left,
      		.pagedjs_margin-left-bottom,
      		.pagedjs_margin-left-middle,
      		.pagedjs_margin-left-top,
      		.pagedjs_margin-right,
      		.pagedjs_margin-right-bottom,
      		.pagedjs_margin-right-middle,
      		.pagedjs_margin-right-top,
      		.pagedjs_margin-top,
      		.pagedjs_margin-top-center,
      		.pagedjs_margin-top-left,
      		.pagedjs_margin-top-left-corner-holder,
      		.pagedjs_margin-top-right,
      		.pagedjs_margin-top-right-corner-holder {
      			box-shadow: 0 0 0 1px inset var(--color-marginBox)
      		}
      	}
      </style>

      <!-- Scripts -->
      <script src="https://unpkg.com/pagedjs/dist/paged.polyfill.js"></script>
      <script type="text/javascript" src="js/regex-typo"></script>
      <script type="text/javascript" src="js/imposition.">

        function insertPagedJS(){
            let $script = document.createElement('script');

            // $script.setAttribute('src', 'paged.polyfill.js');
            // $script.setAttribute('src', 'https://unpkg.com/pagedjs/dist/paged.polyfill.js');
            // window.PagedConfig = {auto: false};

            $script.onload = (e) => {
                let $impoScript = document.createElement('script');
                $impoScript.setAttribute('src', 'imposition.js');
                //ACTIVER LA LIGNE SUIVANTE POUR LANCER L'IMPOSITION
                document.querySelector('head').appendChild($impoScript);
                setTimeout(() => {
                    window.PagedPolyfill.preview();
                }, 1000);

            };

            document.querySelector('head').appendChild($script);
        }

      </script>
   </head>
  <body>
